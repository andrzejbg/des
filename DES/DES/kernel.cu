
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <inttypes.h>
using namespace std;

#define BLOCK_SIZE	1024
#define BLOCK_COUNT	2048
#define MSG_LEN 1

__device__ bool running = true;

// Host matrices:
const int PC1[56] = {
	57, 49, 41, 33, 25, 17,  9,
	1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27,
	19, 11,  3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29,
	21, 13,  5, 28, 20, 12,  4
};
const int Rotations[16] = {
	1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};
const int PC2[48] = {
	14, 17, 11, 24,  1,  5,
	3, 28, 15,  6, 21, 10,
	23, 19, 12,  4, 26,  8,
	16,  7, 27, 20, 13,  2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};
const int InitialPermutation[64] = {
	58, 50, 42, 34, 26, 18, 10,  2,
	60, 52, 44, 36, 28, 20, 12,  4,
	62, 54, 46, 38, 30, 22, 14,  6,
	64, 56, 48, 40, 32, 24, 16,  8,
	57, 49, 41, 33, 25, 17,  9,  1,
	59, 51, 43, 35, 27, 19, 11,  3,
	61, 53, 45, 37, 29, 21, 13,  5,
	63, 55, 47, 39, 31, 23, 15,  7
};
const int FinalPermutation[64] = {
	40,  8, 48, 16, 56, 24, 64, 32,
	39,  7, 47, 15, 55, 23, 63, 31,
	38,  6, 46, 14, 54, 22, 62, 30,
	37,  5, 45, 13, 53, 21, 61, 29,
	36,  4, 44, 12, 52, 20, 60, 28,
	35,  3, 43, 11, 51, 19, 59, 27,
	34,  2, 42, 10, 50, 18, 58, 26,
	33,  1, 41,  9, 49, 17, 57, 25
};
const int DesExpansion[48] = {
	32,  1,  2,  3,  4,  5,  4,  5,
	6,  7,  8,  9,  8,  9, 10, 11,
	12, 13, 12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21, 20, 21,
	22, 23, 24, 25, 24, 25, 26, 27,
	28, 29, 28, 29, 30, 31, 32,  1
};
const int DesSbox[8][4][16] = {
	{
		{ 14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7 },
		{ 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8 },
		{ 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0 },
		{ 15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13 },
	},

	{
		{ 15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10 },
		{ 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5 },
		{ 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15 },
		{ 13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9 },
	},

	{
		{ 10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8 },
		{ 13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1 },
		{ 13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7 },
		{ 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12 },
	},

	{
		{ 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15 },
		{ 13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9 },
		{ 10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4 },
		{ 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14 },
	},

	{
		{ 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9 },
		{ 14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6 },
		{ 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14 },
		{ 11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3 },
	},

	{
		{ 12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11 },
		{ 10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8 },
		{ 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6 },
		{ 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13 },
	},

	{
		{ 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1 },
		{ 13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6 },
		{ 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2 },
		{ 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12 },
	},

	{
		{ 13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7 },
		{ 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2 },
		{ 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8 },
		{ 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11 },
	},
};
const int Pbox[32] = {
	16,  7, 20, 21, 29, 12, 28, 17,
	1, 15, 23, 26,  5, 18, 31, 10,
	2,  8, 24, 14, 32, 27,  3,  9,
	19, 13, 30,  6, 22, 11,  4, 25
};

// Device matrices:
__constant__ int d_PC1[56] = {
	57, 49, 41, 33, 25, 17,  9,
	1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27,
	19, 11,  3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29,
	21, 13,  5, 28, 20, 12,  4
};
__constant__ int d_Rotations[16] = {
	1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};
__constant__ int d_PC2[48] = {
	14, 17, 11, 24,  1,  5,
	3, 28, 15,  6, 21, 10,
	23, 19, 12,  4, 26,  8,
	16,  7, 27, 20, 13,  2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};
__constant__ int d_InitialPermutation[64] = {
	58, 50, 42, 34, 26, 18, 10,  2,
	60, 52, 44, 36, 28, 20, 12,  4,
	62, 54, 46, 38, 30, 22, 14,  6,
	64, 56, 48, 40, 32, 24, 16,  8,
	57, 49, 41, 33, 25, 17,  9,  1,
	59, 51, 43, 35, 27, 19, 11,  3,
	61, 53, 45, 37, 29, 21, 13,  5,
	63, 55, 47, 39, 31, 23, 15,  7
};
__constant__ int d_FinalPermutation[64] = {
	40,  8, 48, 16, 56, 24, 64, 32,
	39,  7, 47, 15, 55, 23, 63, 31,
	38,  6, 46, 14, 54, 22, 62, 30,
	37,  5, 45, 13, 53, 21, 61, 29,
	36,  4, 44, 12, 52, 20, 60, 28,
	35,  3, 43, 11, 51, 19, 59, 27,
	34,  2, 42, 10, 50, 18, 58, 26,
	33,  1, 41,  9, 49, 17, 57, 25
};
__constant__ int d_DesExpansion[48] = {
	32,  1,  2,  3,  4,  5,  4,  5,
	6,  7,  8,  9,  8,  9, 10, 11,
	12, 13, 12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21, 20, 21,
	22, 23, 24, 25, 24, 25, 26, 27,
	28, 29, 28, 29, 30, 31, 32,  1
};
__constant__ int d_DesSbox[8][4][16] = {
	{
		{ 14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7 },
		{ 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8 },
		{ 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0 },
		{ 15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13 },
	},

	{
		{ 15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10 },
		{ 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5 },
		{ 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15 },
		{ 13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9 },
	},

	{
		{ 10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8 },
		{ 13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1 },
		{ 13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7 },
		{ 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12 },
	},

	{
		{ 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15 },
		{ 13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9 },
		{ 10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4 },
		{ 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14 },
	},

	{
		{ 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9 },
		{ 14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6 },
		{ 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14 },
		{ 11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3 },
	},

	{
		{ 12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11 },
		{ 10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8 },
		{ 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6 },
		{ 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13 },
	},

	{
		{ 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1 },
		{ 13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6 },
		{ 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2 },
		{ 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12 },
	},

	{
		{ 13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7 },
		{ 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2 },
		{ 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8 },
		{ 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11 },
	},
};
__constant__ int d_Pbox[32] = {
	16,  7, 20, 21, 29, 12, 28, 17,
	1, 15, 23, 26,  5, 18, 31, 10,
	2,  8, 24, 14, 32, 27,  3,  9,
	19, 13, 30,  6, 22, 11,  4, 25
};

void printBits(uint64_t value) {
	uint64_t n = (uint64_t)1 << 63;
	while (n) {
		if (n & value)
			printf("1");
		else
			printf("0");
		n >>= 1;
	}
	printf("\n");
}

__device__ __host__ void generateKeys(uint64_t key, uint64_t subkeys[], const int pc1[], const int rotations[], const int pc2[],
	bool decrypt) {
	uint64_t k = 0;
	uint64_t firstBit = (uint64_t)1 << 63;
	for (int i = 0; i < 56; i++) {
		k += ((firstBit >> (pc1[i] - 1)) & key) > 0 ? (uint64_t)1 << (55 - i) : 0;
	}

	uint64_t c[17];
	uint64_t d[17];

	const uint64_t mask = 0x000000000FFFFFFF;
	c[0] = (k & (mask << 28)) >> 28;
	d[0] = k & mask;

	int bit29 = 1 << 28;
	int bit30 = 1 << 29;

	for (int i = 1; i < 17; i++) {
		int shifts = rotations[i - 1];
		c[i] = c[i - 1] << shifts;
		d[i] = d[i - 1] << shifts;
		if (c[i] & bit29) {
			c[i] += 1;
		}
		if (d[i] & bit29) {
			d[i] += 1;
		}
		if (shifts == 2) {
			if (c[i] & bit30) {
				c[i] += 2;
			}
			if (d[i] & bit30) {
				d[i] += 2;
			}
		}
		c[i] &= mask;
		d[i] &= mask;

		subkeys[i - 1] = 0;
		uint64_t concatenatedKey = (c[i] << 28) | d[i];
		uint64_t first56bit = firstBit >> 8;
		for (int j = 0; j < 48; j++) {
			subkeys[i - 1] += ((first56bit >> (pc2[j] - 1)) & concatenatedKey) > 0 ? (uint64_t)1 << (47 - j) : 0;
		}
	}

	if (decrypt) {
		for (int i = 0; i <= 7; i++)
		{
			uint64_t tmp = subkeys[i];
			subkeys[i] = subkeys[15 - i];
			subkeys[15 - i] = tmp;
		}
	}
}

__device__ __host__ uint64_t sFunc(uint64_t block, const int sboxes[4][16]) {
	uint64_t firstBit = 32;
	uint64_t lastBit = 1;
	uint64_t middleBits = 30;

	int column = 0, row = 0;
	if (firstBit & block)
		row += 2;
	if (lastBit & block)
		row += 1;
	column = (middleBits & block) >> 1;

	return (uint64_t)sboxes[row][column];
}

__device__ __host__ void encryptBlock(uint64_t msg[], int idx, uint64_t keys[], uint64_t result[], const int ip[],
	const int selection[], const int sboxes[8][4][16], const int p[], const int invertedIp[]) {
	uint64_t firstBit = (uint64_t)1 << 63;
	uint64_t first32bit = firstBit >> 32;
	uint64_t mask = 0x00000000FFFFFFFF;
	result[idx] = 0;
	for (int j = 0; j < 64; j++) {
		result[idx] += ((firstBit >> (ip[j] - 1)) & msg[idx]) > 0 ? (uint64_t)1 << (63 - j) : 0;
	}

	uint64_t l[17];
	uint64_t r[17];
	l[0] = (result[idx] & (mask << 32)) >> 32;
	r[0] = result[idx] & mask;


	for (int j = 1; j < 17; j++) {
		l[j] = r[j - 1];
		r[j] = l[j - 1];

		uint64_t expandedR = 0;
		for (int k = 0; k < 48; k++) {
			expandedR += ((first32bit >> (selection[k] - 1)) & r[j - 1]) > 0 ? (uint64_t)1 << (47 - k) : 0;
		}

		expandedR ^= keys[j - 1];


		uint64_t sOutput = 0;
		for (int k = 0; k < 8; k++) {
			sOutput += ((sFunc(expandedR >> (42 - k * 6), sboxes[k])) << (28 - k * 4));
		}

		uint64_t permutedOutput = 0;
		for (int k = 0; k < 32; k++) {
			permutedOutput += ((first32bit >> (p[k] - 1)) & sOutput) > 0 ? (uint64_t)1 << (31 - k) : 0;
		}

		r[j] ^= permutedOutput;
	}

	uint64_t message = (r[16] << 32) | l[16];
	result[idx] = 0;
	for (int j = 0; j < 64; j++) {
		result[idx] += ((firstBit >> (invertedIp[j] - 1)) & message) > 0 ? (uint64_t)1 << (63 - j) : 0;
	}
}

__device__ __host__ void encryptMessage(uint64_t msg[], uint64_t key, uint64_t result[], const int pc1[], const int rotations[], const int pc2[], const int ip[],
	const int selection[], const int sboxes[8][4][16], const int p[], const int invertedIp[], bool decrypt) {
	uint64_t subkeys[16];
	generateKeys(key, subkeys, pc1, rotations, pc2, decrypt);
	for (int i = 0; i < MSG_LEN; i++)
		encryptBlock(msg, i, subkeys, result, ip, selection, sboxes, p, invertedIp);
}

__global__ void crackDes(uint64_t msg[], uint64_t result[], int knownZeros, uint64_t originalMsg[], uint64_t maxKey) {
	uint64_t threadId = blockIdx.x * BLOCK_SIZE + threadIdx.x;
	uint64_t mask = (uint64_t)127;
	uint64_t decryptedMsg[MSG_LEN];
	uint64_t key = 0;
	uint64_t suffix = ((threadId & (mask << 14)) << 3) | ((threadId & (mask << 7)) << 2) | ((threadId & mask) << 1);

	for (uint64_t i = 0; i < maxKey && running; i++) {
		bool isCorrect = true;
		key = (((i & (mask << 28)) << 5) | (((i & (mask << 21)) << 4) | ((i & (mask << 14)) << 3) | ((i & (mask << 7)) << 2) | ((i & mask) << 1)) << 24) | suffix;
		encryptMessage(msg, key, decryptedMsg, d_PC1, d_Rotations, d_PC2, d_InitialPermutation, d_DesExpansion, d_DesSbox, d_Pbox, d_FinalPermutation, true);

		for (int j = 0; j < MSG_LEN; j++) {
			if (decryptedMsg[j] != originalMsg[j]) {
				isCorrect = false;
				break;
			}
		}

		if (isCorrect) {
			for (int j = 0; j < MSG_LEN; j++) {
				result[j] = decryptedMsg[j];
			}
			running = false;
			break;
		}
	}
}

__global__ void setRunning() {
	running = true;
}

cudaError_t cudaDes(uint64_t msg[], uint64_t result[], int knownZeros, uint64_t originalMsg[], uint64_t maxKey)
{
	uint64_t *d_msg;
	uint64_t *d_originalMsg;
	uint64_t *d_result;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	// Allocate GPU buffers for three vectors (two input, one output)    .
	cudaStatus = cudaMalloc((void**)&d_msg, MSG_LEN * sizeof(uint64_t));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&d_originalMsg, MSG_LEN * sizeof(uint64_t));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&d_result, MSG_LEN * sizeof(uint64_t));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// Copy input vectors from host memory to GPU buffers.
	cudaStatus = cudaMemcpy(d_msg, msg, MSG_LEN * sizeof(uint64_t), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaStatus = cudaMemcpy(d_originalMsg, originalMsg, MSG_LEN * sizeof(uint64_t), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaStatus = cudaMemset(d_result, 0, MSG_LEN * sizeof(uint64_t));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemset failed!");
		goto Error;
	}

	setRunning << < 1, 1>> >();

	// Launch a kernel on the GPU
	crackDes << <BLOCK_COUNT, BLOCK_SIZE>> >(d_msg, d_result, knownZeros, d_originalMsg, maxKey);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "crackDes launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching crackDes!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(result, d_result, MSG_LEN * sizeof(uint64_t), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

Error:
	cudaFree(d_msg);
	cudaFree(d_result);
	cudaFree(d_originalMsg);

	return cudaStatus;
}


int main()
{
	uint64_t key = 0b0000000000000000000000000000000000001110111111101111111011111110;
	int knownZeros = 36;
	uint64_t msg[MSG_LEN] = { 0b0000000100100011010001010110011110001001101010111100110111101111 };
	uint64_t encrypted[MSG_LEN];

	printf("Known zeros: %d\n", knownZeros);
	printf("Message: ");
	for (int i = 0; i < MSG_LEN; i++)
	{
		printBits(msg[i]);
	}
	printf("Key: ");
	printBits(key);

	encryptMessage(msg, key, encrypted, PC1, Rotations, PC2, InitialPermutation, DesExpansion, DesSbox, Pbox, FinalPermutation, false);

	printf("Encrypted message: \n");

	for (int i = 0; i < MSG_LEN; i++) {
		printBits(encrypted[i]);
	}

	uint64_t decrypted[MSG_LEN];

	uint64_t maxKey = (uint64_t)1 << (35 - (knownZeros - knownZeros / 8));
	if (maxKey <= 0)
		maxKey = 1;

	printf("------------------------------------------- GPU: ------------------------------------------------\n");

	clock_t start = clock();

	cudaDes(encrypted, decrypted, knownZeros, msg, maxKey);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;

	printf("Decrypted: \n");

	bool isCorrect = true;
	for (int i = 0; i < MSG_LEN; i++) {
		printBits(decrypted[i]);
		if (decrypted[i] != msg[i])
			isCorrect = false;
	}

	printf("Result is %s \n", isCorrect ? "correct" : "incorrect");

	printf("Time: %f seconds\n", seconds);

	printf("------------------------------------------- CPU: ------------------------------------------------\n");

	start = clock();

	uint64_t mask = (uint64_t)127;

	maxKey = (uint64_t)1 << 58;
	if (maxKey <= 0)
		maxKey = 1;

	for (uint64_t i = 0; i < maxKey; i++) {
		   isCorrect = true;
		uint64_t testKey = (((i & (mask << 49)) << 8) | ((i & (mask << 42)) << 7) | ((i & (mask << 35)) << 6) | ((i & (mask << 28)) << 5) |
			((i & (mask << 21)) << 4) | ((i & (mask << 14)) << 3) | ((i & (mask << 7)) << 2) | ((i & mask) << 1));

		encryptMessage(encrypted, testKey, decrypted, PC1, Rotations, PC2, InitialPermutation, DesExpansion, DesSbox, Pbox, FinalPermutation, true);

		for (int j = 0; j < MSG_LEN; j++) {
			if (decrypted[j] != msg[j]) {
				isCorrect = false;
				break;
			}
		}

		if (isCorrect) {
			break;
		}
	}

	end = clock();
	seconds = (float)(end - start) / CLOCKS_PER_SEC;

	isCorrect = true;
	for (int i = 0; i < MSG_LEN; i++) {
		printBits(decrypted[i]);
		if (decrypted[i] != msg[i])
			isCorrect = false;
	}

	printf("Result is %s \n", isCorrect ? "correct" : "incorrect");

	printf("Time: %f seconds\n", seconds);

	printf("-------------------------------------------------------------------------------------------------\n");

	getchar();
    return 0;
}