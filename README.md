# DES brute force

Implementation of CUDA assignment: brute force cracking of DES encryption. Tries to decrypt message with all possible keys, stops when decrypted message is the same as original.
Includes plot for some lengths of the key, tested on GeForce GTX 750 TI.


Works with CUDA 8